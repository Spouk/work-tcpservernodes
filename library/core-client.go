package library

import (
	"encoding/gob"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

type Client struct {
	srv SrvInfo
}

func NewClient(srv SrvInfo) *Client {
	cc := &Client{srv: srv}
	return cc
}
func (c *Client) Connect() {
	log.Println(c.srv.ToString())
	//создание подключения
	conn, err := net.Dial("tcp", c.srv.ToString())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	//формирую кодер/декодер для передачи байтов через канал
	enc := gob.NewEncoder(conn) //toServer
	dec := gob.NewDecoder(conn) //fromServer

	//формирую первичный пакет серверу
	firtsPacket := TcpPacket{
		From: SrvInfo{
			Port: 0,
			Addr: "127.0.0.3",
			Net:  "tcp",
			Name: "test1",
		},
		To:      SrvInfo{},
		Command: "first",
		Databox: DataboxUpload{},
	}
	//передаю первый пакет на сторону сервера
	err = enc.Encode(&firtsPacket)
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("[client] success send first packet to server  `%v`\n", firtsPacket)

	var answerPacket TcpPacket
	err = dec.Decode(&answerPacket)
	if err != nil {
		log.Fatalln(err)
	}
	if answerPacket.Command != "ok" {
		log.Printf("command from server not = `ok`, exit\n")
		return
	}
	log.Printf("[client] success GET first packet FROM server  `%v`\n", answerPacket)

	// переменные для передачи файла
	var fout *os.File
	inbuf := make(map[int][]byte)

	//режим обработки пакетов от сервера
	for {
		//жду приема пакета от сервера
		var answerPacket TcpPacket
		err = dec.Decode(&answerPacket)
		if err != nil {
			if err == io.EOF {
				log.Printf("[eof] error read packet from server: %v\n", err)
				log.Fatalf("error: connection remote closed, exit\n")
				return
			} else {
				log.Printf("error read packet from server: %v\n", err)
			}
		} else {
			//log.Printf("success get packet from server `%v`\n", answerPacket)
			switch answerPacket.Command {
			case "upload":
				log.Printf("command -> upload file\n")

				switch answerPacket.Databox.CurrentStatus {
				//первый пакет, информационный
				case 1:
					//обнуляю массив выходных данных
					inbuf = make(map[int][]byte)
					//создаю выходной файл + выходной буфер
					fpath := strings.Join([]string{answerPacket.Databox.RemotePath, answerPacket.Databox.RemoteName}, "/")
					fout1, err := os.OpenFile(fpath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, os.ModePerm)
					if err != nil {
						log.Printf("error open output file: %v\n", err)
						continue
					}
					fout = fout1
					//передача чанков файла
				case 2:
					//получение чанков
					//t := make([]byte, len(answerPacket.Databox.Data))
					//inbuf[answerPacket.Databox.ChunkNumber] = t
					//copy(inbuf[answerPacket.Databox.ChunkNumber], answerPacket.Databox.Data)

					_, _ = fout.Write(answerPacket.Databox.Data)

					//log.Printf("[2] Length: %v : %v\n", len(answerPacket.Databox.Data), len(inbuf))
					//завершающий этап - последний чанк
				case 3:
					//log.Printf("[3] Length: %v : %v\n", len(answerPacket.Databox.Data), len(inbuf))
					log.Printf("[3]\n")
					//t := make([]byte, len(answerPacket.Databox.Data))
					//inbuf[answerPacket.Databox.ChunkNumber] = t
					//copy(inbuf[answerPacket.Databox.ChunkNumber], answerPacket.Databox.Data)

					_, _ = fout.Write(answerPacket.Databox.Data)

					//проверка хэша
					totalLen := 0
					for _, v := range inbuf {
						totalLen = totalLen + len(v)
					}
					hash := answerPacket.Databox.GetHashFileSize(int64(totalLen))
					log.Printf("Hash: `%s` : `%s` \nlengnth buffer: %4d  : %4d\n", answerPacket.Databox.Hash, hash, len(inbuf), answerPacket.Databox.Size)
					for _, v := range inbuf {
						count, err := fout.Write(v)
						if err != nil {
							log.Printf("Error write to file: %v\n", err)
							continue
						}
						log.Printf("success write to file `%v`\n", count)

					}
					_ = fout.Close()
				}

				//if hash == answerPacket.Databox.Hash {
				//	//хэш совпадает, сохраняю файл
				//	count, err := fout.Write(inbuf)
				//	if err != nil {
				//		log.Printf("Error write to file: %v\n", err)
				//		continue
				//	}
				//	log.Printf("success write to file `%v`\n", count)
				//	_ = fout.Close()
				//}

			case "exit":
				log.Printf("command -> exit\n")
				return
			}
		}
	}
}
