package library

import (
	"crypto/sha1"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"
)

//-----------------------------------------------------------------------------
type SrvInfo struct {
	Port int    // 3333
	Addr string //127.0.0.1
	Net  string // tcp, udp
	Name string
}

func (s SrvInfo) ToString() string {
	return fmt.Sprintf("%s:%d", s.Addr, s.Port)
}
func (s SrvInfo) Hash() string {
	ss := sha1.New()
	ss.Write([]byte(fmt.Sprintf("%s:%s:%s", s.Net, s.Addr, s.Port)))
	return hex.EncodeToString(ss.Sum(nil))
}

//-----------------------------------------------------------------------------
type TcpPacket struct {
	From     SrvInfo
	To       SrvInfo
	Command  string
	Filename string
	Databox  DataboxUpload
}
type UdpPacket struct {
	From    SrvInfo
	To      SrvInfo
	Command string
	Databox interface{}
}

//-----------------------------------------------------------------------------
type DataboxUpload struct {
	Filename      string
	Path          string
	Hash          string //хэш по размеру файла
	Size          int64
	RemoteName    string
	RemotePath    string
	ChunkNumber   int
	Data          []byte
	BufferSize    int64
	CurrentStatus int //текущий статус передачи 1 информация о файле, 2 передача файла по чанкам, 3 последний чанк
}

//возвращает количество чанков в передавемом файле
func (d *DataboxUpload) SplitChankFile(s, sizeBuf int64) int {
	var result = -1
	if s%sizeBuf > 0 {
		result = int((s / sizeBuf) + 1)
	} else {
		result = int(s / sizeBuf)
	}
	return result
}
func (d *DataboxUpload) GetHashFileSize(s int64) string {
	h := sha1.New()
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(s))
	_, err := h.Write(b)
	if err != nil {
		log.Printf("error %v\n", err)
		return ""
	}
	hexSize := hex.EncodeToString(h.Sum(nil))
	return fmt.Sprintf("%s", hexSize)
}
