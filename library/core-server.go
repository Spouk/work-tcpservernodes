//----------------------------------------------------------------------
// TODO: использовать 2 порта для создание взаимной связи клиент-сервер
// между агентом и корневым сервером раздачи
// зачем агенту нужна серверная часть?
// 	- уведомлять сервер о активности агента + осуществлять передачу данных
//    по выполянемым задачам, включая возможность передачи файлов от агента корневому серверу,
//	  обеспечение полноценной двусторонней связи (дуплекс)
//
//-----------------------------------------------------------------------
package library

import (
	"encoding/gob"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"sync"
	"time"
)

type Server struct {
	srv   SrvInfo
	sw    sync.WaitGroup
	chTCP chan TcpPacket
	chUDP chan UdpPacket
	stock map[string]SrvInfo
	count int
	sync.RWMutex
}

func NewServer(srv SrvInfo) *Server {
	s := &Server{
		srv:   srv,
		chTCP: make(chan TcpPacket),
		chUDP: make(chan UdpPacket),
		stock: make(map[string]SrvInfo),
	}
	return s
}

// extChCommand - "ниточка" для передачи команд управления базовому серверу откуда-то извне, к примеру о прекращении работы
func (s *Server) Start(extChCommand chan string) {
	fromManagerTCP := make(chan TcpPacket)

	go s.RunTCP(fromManagerTCP)

	timeUpload := time.After(time.Second * 15)

	for {
		select {
		case command := <-extChCommand:
			if command == "exit" {
				log.Printf("--found external command for exit programm, exit\n")
				return
			} else {
				log.Printf("[extChCommand] not validate command `%s`\n", command)
			}
		case t := <-s.chTCP:
			log.Printf("[tcpChan] %v\n", t)
		case u := <-s.chUDP:
			log.Printf("[udpChan] %v\n", u)
		case _ = <-timeUpload:
			log.Println("timaAfter UPLOAD\n")
			db := DataboxUpload{}
			stat, err := os.Stat("/tmp/fileserver.file")
			if err != nil {
				log.Printf("error get stat file: %v\n", err)
			} else {
				stat.Size()
				db.Size = stat.Size()
				db.Filename = "/tmp/fileserver.file"
				db.Path = "/tmp/"
				db.RemoteName = stat.Name()
				db.RemotePath = "/tmp/fromclient"
			}
			fromManagerTCP <- TcpPacket{
				From:    SrvInfo{},
				To:      SrvInfo{},
				Command: "upload",
				Databox: db,
			}
		}
	}
}

//основной tcpServer
func (s *Server) RunTCP(fromManager chan TcpPacket) {
	//запускаю сервер tcp
	tcpC, err := net.Listen("tcp4", s.srv.ToString())
	if err != nil {
		log.Fatalln(err)
	}
	//запуск менеджера по работе с внешними коннектами
	chNewc := make(chan io.ReadWriteCloser)
	go s.Manager(chNewc, fromManager)

	//цикл обработки внешних поступлений
	for {
		newc, err := tcpC.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		log.Printf("found new connection `%v`\n", newc.RemoteAddr())
		chNewc <- newc
	}
}

// менеджер горутин
func (s *Server) Manager(chNewC chan io.ReadWriteCloser, fromManager chan TcpPacket) {
	defer func() {
		log.Println("manager exit...")
	}()
	chToWorker := make(chan TcpPacket)
	toClose := make(chan bool)

	for {
		select {
		//новое коннект
		case newConnect := <-chNewC:
			s.Lock()
			s.count++
			s.Unlock()
			log.Printf("[manager] found new connection `%v`\n", newConnect)
			go s.Worker(newConnect, chToWorker, toClose)
		case fromManagerCommand := <-fromManager:
			if fromManagerCommand.Command == "exit" {
				log.Println("[manager] from core manager command `exit` exit")
				//оповещаю воркеров о закрытии лавочки
				close(toClose)
				return
			}
			//рассылка команд по нодам
			log.Printf("Count connections nodes `%d`\n", s.count)
			for i := 0; i < s.count; i++ {
				chToWorker <- fromManagerCommand
			}
		}
	}

}

func (s *Server) Worker(c io.ReadWriteCloser, toWorker chan TcpPacket, toClose chan bool) {
	defer func() {
		log.Printf("worker `%s` the end work\n", "dfgdfg")
		_ = c.Close()
	}()
	log.Printf("[worker] found new connection `%v`\n", c)

	enc := gob.NewEncoder(c) //to
	dec := gob.NewDecoder(c) //from

	//жду первого пакета от клиента
	var fromClient TcpPacket
	err := dec.Decode(&fromClient)
	if err != nil {
		log.Printf("error get first correct packet from client, exit\n")
		return
	}
	log.Printf("[worker] get first packet from client `%v`\n", fromClient)
	//изменяю счетчик количества подключений к серверу
	s.Lock()
	//s.count++
	s.stock[fromClient.From.Hash()] = fromClient.From
	s.Unlock()

	//передаю ответ клиенту
	toClient := TcpPacket{
		From: SrvInfo{
			Port: 7777,
			Addr: "127.0.0.1",
			Net:  "tcp",
			Name: "server",
		},
		To:      fromClient.From,
		Command: "ok",
		Databox: DataboxUpload{},
	}
	err = enc.Encode(&toClient)
	if err != nil {
		log.Printf("Error send packet to client `%v`\n", err)
		return
	}
	log.Printf("[worker] success send packet to lient `%v`\n", toClient)
	var flagUpload = false

	for {
		select {
		case _ = <-toClose:
			log.Println("FOUND TERMINATED!!!")
			return
		case com := <-toWorker:
			log.Printf("getting command from manager `%v`\n", com)
			//upload file to Client
			if com.Command == "upload" {
				if flagUpload {
					log.Printf("already upload file this worker now!!!\n")
				} else {
					dbox := com.Databox
					flagUpload = true
					log.Printf("start upload file to client\n")
					stat, err := os.Stat(dbox.Filename)
					if err != nil {
						fmt.Printf("[error open file] %v\n", err)
						flagUpload = false
						continue
					} else {
						if stat.IsDir() {
							fmt.Printf("[error open file]  its no file, its directory `%v`\n", com.Filename)
							flagUpload = false
							continue
						} else {
							f, err := os.Open(dbox.Filename)
							if err != nil {
								fmt.Printf("[error open file] %v\n", err)
								flagUpload = false
								continue
							} else {

								//формирую очередь для последующей отправки файла по чанкам
								sizeBuffer := 1024 * 10
								countChunks := dbox.SplitChankFile(stat.Size(), int64(sizeBuffer))

								bx := DataboxUpload{
									CurrentStatus: 1,
									Filename:      dbox.Filename,
									Path:          dbox.Path,
									Hash:          dbox.GetHashFileSize(stat.Size()),
									Size:          stat.Size(),
									RemoteName:    dbox.RemoteName,
									RemotePath:    dbox.RemotePath,
									BufferSize:    int64(sizeBuffer * countChunks),
								}

								//сперва передаю первый пакет
								newbox := TcpPacket{
									Command: "upload",
									Databox: bx,
								}
								err = enc.Encode(&newbox)
								if err != nil {
									log.Printf("error send data to client...\n")
									continue
								}

								//изменяю флаг для последующей передачи
								bx.CurrentStatus = 2

								for i := 0; i < countChunks; i++ {
									tmp := make([]byte, sizeBuffer)
									count, err := f.Read(tmp)
									if err != nil {
										log.Printf("error read file %v\n", err)
										continue
									} else {
										bx.ChunkNumber = i
										//log.Printf("Size count: %v : %v\n", count, len(siz))
										if count == sizeBuffer {
											//bx.Data = make([]byte, sizeBuffer)
											//copy(bx.Data, tmp)
										} else {
											//последний чанк
											bx.CurrentStatus = 3
											//bx.Data = make([]byte, len(tmp[:count]))
											//copy(bx.Data, tmp[:count])
										}
										bx.Data = make([]byte, len(tmp[:count]))
										copy(bx.Data, tmp[:count])
										log.Printf("Length: %v : %v\n", len(bx.Data), bx.Size)

										newbox := TcpPacket{
											Command: "upload",
											Databox: bx,
										}
										//log.Printf("newBox--- : %v\n", newbox)
										//передача пакета с данными на сторону клиента
										err = enc.Encode(&newbox)
										if err != nil {
											log.Printf("error send data to client...\n")
										} else {
											log.Printf("\rsend file %s.....[%d:%d]", stat.Name(), i, countChunks)
										}
									}
								}
							}
						}
					}
				}

				toClient := TcpPacket{
					Command: "upload",
					Databox: DataboxUpload{},
				}
				err = enc.Encode(&toClient)
				if err != nil {
					log.Printf("Error send packet to client `%v`\n", err)
					return
				}
				log.Printf("success send client packet with command\n`")
			}
		}
	}
}
