package main

import (
	"gitlab.com/work-tcpservernodes/library"
)

func main() {
	serv := library.NewServer(library.SrvInfo{
		Port: 7777,
		Addr: "127.0.0.1",
		Net:  "tcp4",
	})
	ch := make(chan string)
	serv.Start(ch)
}
