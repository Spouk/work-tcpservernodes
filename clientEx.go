package main

import (
	"gitlab.com/work-tcpservernodes/library"
)

func main() {
	cc := library.NewClient(library.SrvInfo{
		Port: 7777,
		Addr: "127.0.0.1",
		Net:  "tcp",
	})
	cc.Connect()
}
