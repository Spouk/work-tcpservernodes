package main

import (
	"log"
	"time"
)

func main() {
	newt := time.NewTicker(time.Second * 3)
	ch := make(chan time.Time)
	go w(ch)

	endTimer := time.After(time.Second * 10)

	for {
		select {
		case _ = <-time.After(time.Second * 5):
			log.Printf("time 5\n")
		case _ = <-endTimer:
			log.Printf("time 10\n")
		case tt := <-ch:
			log.Printf("timer: [t] %v\n", tt)
		case tt2 := <-newt.C:
			log.Printf("timer: [tt2] %v\n", tt2)
		}

	}
}

func w(ch chan time.Time) {
	timer := time.NewTicker(time.Second * 1)
	for {
		select {
		case t := <-timer.C:
			ch <- t
		}
	}
}
