package library

import (
	"io"
	"log"
	"net"
	"sync"
)

type ServerTest struct {
	srv   SrvInfo
	count int
	sync.RWMutex
}

func NewServerTester(srv SrvInfo) *ServerTest {
	c := &ServerTest{
		srv: srv,
	}
	return c
}
func (s *ServerTest) Run() {
	log.Printf("starting tester server on `%v`\n", s.srv)
	cc, err := net.Listen("tcp4", s.srv.ToString())
	if err != nil {
		log.Fatalln(err)
	}
	chCommand := make(chan TcpPacket)
	for {
		con, err := cc.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		s.count++
		go s.Worker(con, chCommand)
	}
}

func (s *ServerTest) Worker(c io.ReadWriteCloser, chCommand chan TcpPacket) {
	defer func() {
		_ = c.Close()
		s.Lock()
		s.count--
		s.Unlock()
	}()
	log.Printf("%v", c)
	for {
		select {
		case com := <-chCommand:
			if com.Command == "exit" {
				return
			}
			if com.Command == "file" {

			}
		}
	}
}
