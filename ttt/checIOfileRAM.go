//----------------------------------------------------------------------
// проверка работы алгоритмы чтения файла с диска в память и вычисления байтового значения в памяти //-----------------------------------------------------------------------
package main

import (
	"crypto/sha1"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
)

func main() {
	checkHashDir("/tmp")

	f := "/tmp/fileserver.file"
	sizeBuf := 1024 * 100
	stat, err := os.Stat(f)
	if err != nil {
		log.Fatal(err)
	}
	res := SplitChankFile(stat.Size(), int64(sizeBuf))
	fmt.Printf("Res %v\n", res)
	//totalSize := res * (1024*100)
	inBuf := []byte{}

	fin, err := os.OpenFile(f, os.O_RDONLY, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
	defer fin.Close()

	for i := 0; i < res; i++ {
		tmpb := make([]byte, sizeBuf)
		count, err := fin.Read(tmpb)
		if err != nil {
			fmt.Printf("error read block file: [%d] %v\n", i, err)
			return
		} else {
			if count == sizeBuf {
				inBuf = append(inBuf, tmpb...)
			} else {
				inBuf = append(inBuf, tmpb[:count]...)
			}

			fmt.Printf("\rread block %d [%d]...%v : %v :: %v\n", i, count, len(inBuf), len(tmpb))
		}
	}
	fmt.Printf("Size totalbox: %d : %d\n`%v`   `%v`\n", len(inBuf), stat.Size(),
		getHashFileSize(stat.Size()), getHashFileSize(int64(len(inBuf))))

}

func SplitChankFile(s, sizeBuf int64) int {
	var result = -1
	if s%sizeBuf > 0 {
		result = int((s / sizeBuf) + 1)
	} else {
		result = int((s / sizeBuf))
	}
	return result
}

func getHashFileSize(s int64) string {
	h := sha1.New()
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(s))
	_, err := h.Write(b)
	if err != nil {
		log.Printf("error %v\n", err)
		return ""
	}
	hexSize := hex.EncodeToString(h.Sum(nil))
	return fmt.Sprintf("%s", hexSize)
}

func checkHashDir(d string) {
	_ = filepath.Walk(d, func(path string, info fs.FileInfo, err error) error {
		if info.IsDir() == false {
			hashFile := getHashFileSize(info.Size())
			fmt.Printf("%s  `%v`\n", info.Name(), hashFile)
		}
		return nil
	})
}
